package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginSteps {
    WebDriver driver;

    @Given("user is on home page")
    public void user_is_on_home_page() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

        String title = driver.getTitle();
        Assert.assertEquals("OrangeHRM", title);
        System.out.println("Test Passed");

    }

    @When("user enters <username> and <password>")
    public void user_enters_username_and_password() {
        driver.findElement(By.name("username")).sendKeys("Admin");
        driver.findElement(By.name("password")).sendKeys("admin123");


    }

    @When("click on login button")
    public void click_on_login_button() {
        driver.findElement(By.xpath("//button")).click();
    }

    @Then("user should logged on home page")
    public void user_should_logged_on_home_page() {
        String home = driver.getCurrentUrl();
        System.out.println("User landed on homepage :-" + home);

    }


}
